<?php
    session_start();
    if(!isset($_SESSION['log']))
        header('Location: admin.php');

    header('Content-Type: text/html; charset=utf-8');
    require_once 'db_conf.php';
    
    if(isset($_POST['new']))
        header('Location: new_menu.php');

    if(isset($_POST['lang'])){
        $_SESSION['lang'] = $lang = $_POST['lang'];
    }
    else {
        if(isset($_SESSION['lang']))
            $lang = $_SESSION['lang'];
        else $lang = "ru";
    }

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Jumi</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/inputs.css">
		<link rel="stylesheet" href="styles/media.css">
		<link rel="stylesheet" href="styles/slideshow.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <style>
            a{
                text-decoration: none; 
                color: #fff;
            }
            a:hover{
                text-decoration: underline; 
            }
        </style>
	</head>
    <body>
        <main_block style="margin-top: 1vh;">
        <space></space>
            <block>
                <center>
                    <form method=POST>
                        <input type=submit value=ru name=lang>
                        <input type=submit value=lv name=lang>
                    </form>
                </center>
            </block>
        <space></space>
            <block>
                <left>
                    <ul>
                        <?php
                            $con = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
                            $con->query("SET CHARSET UTF-8");
        
                            $result = $con->query("SELECT * FROM menu_".$lang.";");
                            if($result){
                                while($row = $result->fetch_assoc()){
                                    echo "<li><a href=\"update.php?id=".$row['Content_ID']."\">".$row['Menu']."</a>";
                                    echo "<ul>";
                                    $sub_menus = json_decode($row['Sub_menu'], true);
                                    if($sub_menus) foreach ($sub_menus["sub"] as &$value) {
                                       if($value[0] != null && $value[1] != null) echo "<li><a href=\"update.php?sub=true&id=".$value[1]."&menu_id=".$row['ID']."\">".$value[0]."</a></li>";
                                    }
                                    echo "<li><a href=\"new_menu.php?id=".$row['ID']."\">new submenu</a></li>";
                                    echo "</ul></li>";
                                }
                            }
                        ?>
                    </ul>
                    <center>
                        <form method=POST>
                            <input type=submit value=add name=new>
                        </form>
                    </center>
                </left>
                <right>     
                </right>
            </block>
        </main_block>
    </body>
</html>
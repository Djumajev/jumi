<?php
    session_start();
    if(!isset($_SESSION['log']))
        header('Location: admin.php');

    header('Content-Type: text/html; charset=utf-8');
    require_once 'db_conf.php';
    require_once 'types_def.php';

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Jumi</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/inputs.css">
		<link rel="stylesheet" href="styles/media.css">
		<link rel="stylesheet" href="styles/slideshow.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	</head>
    <body>
        <main_block style="margin-top: 1vh;">
        <space></space>
            <block>
            <center>
                <?php
                    $id = "";
                    if(isset($_GET['id']))
                        $id = $_GET['id'];

                    if(!isset($_GET['type'])){
                        echo "<a href=\"?id=$id&type=".strval(LEFT)."\"><img width=50% src=./images/left.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(RIGHT)."\"><img width=50% src=./images/right.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(LEFT-RIGHT)."\"><img width=50% src=./images/left-right.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(LEFT-RIGHT-CENTER)."\"><img width=50% src=./images/left-right-center.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(CENTER)."\"><img width=50% src=./images/center.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(LEFT-CENTER)."\"><img width=50% src=./images/left-center.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(RIGHT-CENTER)."\"><img width=50% src=./images/right-center.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(TEAM)."\"><img width=50% src=./images/meet-team.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(CUSTOM)."\"><img width=50% src=./images/custom.png></a>";
                        echo "<a href=\"?id=$id&type=".strval(GALLERY)."\"><img width=50% src=./images/gallery.png></a>";
                    } else{
                        if($_GET['type'] == strval(LEFT))
                            header("Location: create_left.php?id=$id");
                        elseif($_GET['type'] == strval(RIGHT))
                            header("Location: create_right.php?id=$id");
                        elseif($_GET['type'] == strval(LEFT-RIGHT))
                            header("Location: create_left-right.php?id=$id");
                        elseif($_GET['type'] == strval(LEFT-RIGHT-CENTER))
                            header("Location: create_left-right-center.php?id=$id");
                        elseif($_GET['type'] == strval(CENTER))
                            header("Location: create_center.php?id=$id");
                        elseif($_GET['type'] == strval(LEFT-CENTER))
                            header("Location: create_left-center.php?id=$id");
                        elseif($_GET['type'] == strval(RIGHT-CENTER))
                            header("Location: create_right-center.php?id=$id");
                        elseif($_GET['type'] == strval(TEAM))
                            header("Location: create_meet-team.php?id=$id");
                        elseif($_GET['type'] == strval(CUSTOM))
                            header("Location: create_custom.php?id=$id");
                        elseif($_GET['type'] == strval(GALLERY))
                            header("Location: create_gallery.php?id=$id");
                    }
                ?>
            </center>
            </block>
        </main_block>
    </body>
</html>




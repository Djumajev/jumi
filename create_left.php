<?php 
    header('Content-Type: text/html; charset=utf-8');
    require_once 'db_conf.php';
    require_once 'types_def.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Jumi</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/inputs.css">
		<link rel="stylesheet" href="styles/media.css">
		<link rel="stylesheet" href="styles/slideshow.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	</head>
    <body>
        <main_block style="margin-top: 1vh;">
            <space></space>
                <block>
                    <center>
                        <div id="menu-con" contenteditable="true">Menu name</div>
                    </center>
                </block>

                <space></space>

                <block>
                    <left>
                        <div id="txt1-con" contenteditable="true">Enter some text</div>
                    </left>
                </block>

                <space></space>

                <center>
                    <form method=POST onsubmit="return getContent()">
                        <textarea id=txt1 name=txt1 style="display:none"></textarea>
                        <textarea id=menu name=menu style="display:none"></textarea>
                        <input type=submit value=Save>
                    </form>
                </center>

        </main_block>
        <script>
            function getContent(){
                document.getElementById("txt1").value = document.getElementById("txt1-con").innerHTML;
                document.getElementById("menu").value = document.getElementById("menu-con").innerHTML;
             }
        </script>
    </body>
    <?php
        
        session_start();
        if(!isset($_SESSION['log']))
            header('Location: admin_panel.php');

        if(isset( $_SESSION['lang'])){
            $lang = $_SESSION['lang'];
        }
        else $lang = "ru";
            
        if(isset($_POST['txt1'])){
            $con = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
            $con->query("SET CHARSET UTF-8");
            $str = "
            <space></space>
            <block>
                <left id=content>
                   ". $_POST['txt1']."
                </left>
            </block>
            <space></space>";

            $con->query("INSERT INTO content_".$lang."(Content, type) VALUES('$str', ".strval(LEFT-RIGHT).")");
            if(isset($_GET['id']) && $_GET['id'] > 0){
                $id = $_GET['id'];

                $result = $con->query("SELECT * FROM menu_ru WHERE ID = ".$id.";");
                            
                if($result){
                    while($row = $result->fetch_assoc()){
                        $sub_menus = json_decode($row['Sub_menu'], true);
                        $r = $con->query("SELECT ID FROM content_".$lang." WHERE ID = (SELECT MAX(ID) FROM content_".$lang.")");
                        if($r){
                            while($row2 = $r->fetch_assoc()){
                                if(isset($sub_menus["sub"])){

                                    array_push($sub_menus["sub"], array($_POST['menu'], $row2['ID']));
                                } 
                                else{
                                    $new = array("sub" => array(array($_POST['menu'], $row2['ID'])));
                                    $sub_menus = $new;
                                } 
                                $en_arr = json_encode($sub_menus, JSON_UNESCAPED_UNICODE);
                                $con->query("UPDATE menu_".$lang." SET Sub_menu = '$en_arr' WHERE ID = $id;");
                            }
                        }
                    }
                }
            } else $con->query("INSERT INTO menu_".$lang."(Menu, Sub_menu, Content_ID) VALUES('".$_POST['menu']."', '', (SELECT MAX(ID) FROM content_".$lang."))");;
            $con->close();
            header('Location: admin_panel.php');
        }

    ?>
</html>
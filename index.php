<?php 
    //header('Content-Type: text/html; charset=utf-8');
    require_once 'db_conf.php';

    session_start();
    if(isset($_POST['lang'])){
        $_SESSION['lang'] = $lang = $_POST['lang'];
    }
    else {
        if(isset($_SESSION['lang']))
            $lang = $_SESSION['lang'];
        else $lang = "ru";
    }
?>

<!DOCTYPE html>
<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
		<title>Jumi</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/inputs.css">
		<link rel="stylesheet" href="styles/media.css">
        <link rel="stylesheet" href="styles/meet-team.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<script>
			function menu(){
				var mob_menu = document.getElementsByTagName('mob_menu');
                var menu = document.getElementById('menu');
				
				if(mob_menu[0].className === ""){
					mob_menu[0].className += "hidden";	
                    menu.className = "";	
				}
				else{
                    mob_menu[0].className = "";
                    menu.className += "hidden";
                } 
			}
		</script>
		
		<style>

    stars canvas{
        width: 100%;
        height: 100%;
    }


    stars {
        display: block;
        width: 100vw;
        height: 100vh;
	    z-index: -10;
        position: fixed;
        top: 0;
    }

</style>

		
	</head>

	<body>

    <stars>
        <canvas id="Stars" width=100vw height=100vh>Error :( </canvas>
    </stars>
    
    <script src="starField.js"></script>

        <nav class="navigation" id=nav>
                <logo id=logo>
                    <img src="./images/logo.png" width=300%>
                </logo>

                <center style="text-align: right; width: 70%;" id=icons>
                    <a href="https://www.facebook.com/JumiCentrs/"><img src="./images/face_ico.png" width=50em></img></a>
                    <a href="https://www.instagram.com/explore/locations/242108172/jumi-centrs/"><img src="./images/insta_ico.png" width=40em></img></a>
                </center>

                <menu>
                    <ul>
                        <li><a href="#" onclick="menu()" id="menu">&#9776;</a></li>
                        <form method=POST style="float: right;" action=index.php>
                            <input type=submit value=ru name=lang id=lang_btn>
                            <input type=submit value=lv name=lang id=lang_btn>
                        </form>
                        <?php
                            $con = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
                            //$con->query("SET CHARSET UTF-8");
        
                            $result = $con->query("SELECT * FROM menu_".$lang.";");
                            
                            if($result){
                                while($row = $result->fetch_assoc()){
                                    echo "<li><a href=\"?id=".$row['Content_ID']."\">".$row['Menu']."</a>";
                                    echo "<ul class=\"sub\">";
                                    $sub_menus = json_decode($row['Sub_menu'], true);
                                    if($sub_menus) foreach ($sub_menus["sub"] as &$value) {
                                        if($value[0] != null && $value[1] != null) echo "<li><a href=\"?id=".$value[1]."\">".$value[0]."</a><hr style=\"margin: 0; padding: 0;\"></li>";
                                    }
                                    echo "</ul></li>";
                                }
                            }
                        ?>
                    </ul>
                </menu>

                <mob_menu class="hidden">
                        <a href="#" onclick="menu()" id=burger>&#9776;</a><space></space>
                        <?php
                            $result = $con->query("SELECT * FROM menu_".$lang.";");

                            if($result){
                                while($row = $result->fetch_assoc()){
                                    echo "<a href=\"?id=".$row['Content_ID']."\">".$row['Menu']."</a>";
                                    $sub_menus = json_decode($row['Sub_menu'], true);
                                    if($sub_menus) foreach ($sub_menus["sub"] as &$value) {
                                        if($value[0] != null && $value[1] != null) echo "<br><a style=\"padding-left: 20px;\" href=\"?id=".$value[1]."\">".$value[0]."</a>";
                                    }
                                    echo "<hr>";
                                }
                            }
                        ?>
			    </mob_menu>
        </nav>

        <main_block>
            <?php
                if(isset($_GET['id']))
                    $result = $con->query("SELECT * FROM content_".$lang." WHERE ID = ".$_GET['id'].";");
                else $result = $con->query("SELECT * FROM content_".$lang." LIMIT 1;");

                 if($result){
                     while($row = $result->fetch_assoc()){
                         echo $row['Content'];
                     }
                 }
            ?>
        </main_block>

        <space></space>

        <footer style="position: relative;">
			<left>
                <consult_footer onclick="consult_open()">
                    Консультация
                </consult_footer>
                 <form method=POST>
                    <div style="width: 100%;  margin: auto;">
                        <left style="height: 12vmax;">
                            <input type=text name=name placeholder="Имя">
                            <input type=text name=email placeholder="Email">
                            <input type=text name=phone placeholder="Телефон">
                            <select name=courses>
                                <option value="Компьютерные курсы">Компьютерные курсы</option>
                                <option value="Парикмахер">Парикмахер</option>
                                <option value="Визажист">Визажист</option>
                                <option value="Мастер маникюра">Мастер маникюра</option>
                                <option value="Наращивание ресниц">Наращивание ресниц</option>
                                <option value="Waxing">Waxing</option>
                                <option value="Массаж">Массаж</option>
                            </select>
                        </left>
                        <right style="height: 12vmax; padding-bottom: 2%; padding-top: 1%;">
                            <textarea type=text name=txt placeholder="сообщение"></textarea>
                        </right>
                        <space></sapce>
                        <input style="float: top;" type=submit value=отправить name=send>
                    </div>
                </form>
			</left>
			<right>
				<label style="overflow-x: auto;">Связаться с нами Вы можете:</br>
				Тел.: 67283020, 29299679</br>
				e-mail:<a href="mailto:jumicentrs@inbox.lv">jumicentrs@inbox.lv</a></br>
				Marijas 25, Riga, Latvia</label><br>
				<p style="font-size: 80%; vertical-align: bottom; position: absolute; bottom: 0; right: 0;">SIA "Jumi centrs" © 2011 - 2018 | Все права защищены.</p>
			</right>
		</footer>
        <script>
            var scroll = window.scrollTop;
            var logo = document.getElementById("logo");
            var nav = document.getElementById("nav");
            var icons = document.getElementById("icons");

            window.onscroll = function() {
                if(star_canv.width <= 720){
                    var currentPosition = window.pageYOffset || document.documentElement.scrollTop;
    
                    if(currentPosition > 50){
                        logo.className += " hidden ";
                        icons.className += " hidden ";
                        nav.className += " nav_fixed ";
                    }
                    else { 
                        logo.className = "";
                        nav.className = "";
                        icons.className = "";
                    }
                }
            };
            
            window.onload = function(){
                window.scrollTo(0, 0);
            };
                 
        </script>  
        <p class="hidden">Site created by LborV (B.Djumajev)</p>  
        
        <consult onclick="consult_open()">
            Консультация
        </consult>
        <script>
            function consult_open(){
                document.getElementById("consult_page").className = "";
            }
            function consult_close(){
                document.getElementById("consult_page").className = "hidden";
            }
        </script>

        <consult_page id=consult_page class="hidden">
          <block>
              <space></sapce>
              <space></sapce>
              <space></sapce> 
             <form method=POST>   
                <div style="cursor: pointer; font-size: 200%; width: 20%; float: right; color: #FFF;" onclick="consult_close()">
                    X
                </div>
                <space></space>
                <div style="width: 50%;  margin: auto;">
                   <left style="height: 100%;">
                       <input type=text name=name placeholder="Имя">
                        <input type=text name=email placeholder="Email">
                        <input type=text name=phone placeholder="Телефон">
                        <select name=courses>
                            <option value="Компьютерные курсы">Компьютерные курсы</option>
                            <option value="Парикмахер">Парикмахер</option>
                            <option value="Визажист">Визажист</option>
                            <option value="Мастер маникюра">Мастер маникюра</option>
                            <option value="Наращивание ресниц">Наращивание ресниц</option>
                            <option value="Waxing">Waxing</option>
                            <option value="Массаж">Массаж</option>
                        </select>
                    </left>
                    <right style="height: 10vmax;">
                        <textarea type=text name=txt placeholder="сообщение"></textarea>
                    </right>
                    <space></sapce>
                    <input style="float: right;" type=submit value=отправить name=send>
                    </div>
                 </form>
                 <?php
                     function sendMailRU(){
                        $to      = 'boris.djumajev@gmail.com';
                        $subject = 'Новая заявка на занятие';
                        
                        $message = '
                            <html>
                                <head>
                                    <title>Новая заявка на занятие</title>
                                </head>
                                <body>
                                    Почта: <b>'.$_POST["email"].'</b><br>
                                    Номер: <b>'.$_POST["phone"].'</b><br>
                                    Имя: <b>'.$_POST["name"].'</b><br>
                                    Курс: <b>'.$_POST["courses"].'</b><br>
                                    Сообщение: <b>'.$_POST["txt"].'</b><br>
                                </body>
                            </html>';
                            
                        $headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/html; charset=utf-8\r\n";

                        $_POST = array();
                        if(mail($to, $subject, $message, $headers))
                            echo "<script>alert('Сообщение отправлено!');</script>";
                        
                    }
                    
                    function sendMailLV(){
                        $to      = 'boris.djumajev@gmail.com';
                        $subject = 'Jauns pieteikums par klasi';
                        
                        $message = '
                            <html>
                                <head>
                                    <title>Jauns pieteikums par klasi</title>
                                </head>
                                <body>
                                    Pasts: <b>'.$_POST["email"].'</b><br>
                                    Numurs: <b>'.$_POST["phone"].'</b><br>
                                    Vārds: <b>'.$_POST["name"].'</b><br>
                                    Kurs: <b>'.$_POST["courses"].'</b><br>
                                    Vēstule: <b>'.$_POST["txt"].'</b><br>
                                </body>
                            </html>';
                            
                        $headers = "From: $from\r\nReply-to: $from\r\nContent-type: text/html; charset=utf-8\r\n";

                        $_POST = array();
                        if(mail($to, $subject, $message, $headers))
                            echo "<script>alert('Сообщение отправлено!');</script>";
                        
                    }

                    if(isset($_POST['send']) && $_POST['send'] == "отправить")
                        sendMailRU();
                ?>
          </block>
        </consult_page>
    </body>
</html>

<?php
    $con->close();
?>
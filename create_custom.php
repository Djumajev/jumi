<?php 
    header('Content-Type: text/html; charset=utf-8');
    require_once 'db_conf.php';
    require_once 'types_def.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Jumi</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/inputs.css">
		<link rel="stylesheet" href="styles/media.css">
		<link rel="stylesheet" href="styles/slideshow.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	</head>
    <body>
        <main_block style="margin-top: 1vh;">
            <block>
                <?php
                    $space = htmlspecialchars("<space></space>", ENT_QUOTES);
                    $block = htmlspecialchars("<block></block>", ENT_QUOTES);
                    $left = htmlspecialchars("<left></left>", ENT_QUOTES);
                    $right = htmlspecialchars("<right></right>", ENT_QUOTES);
                    $center = htmlspecialchars("<center></center>", ENT_QUOTES);
                ?>
                <text>Полезные тэги:<?php echo " 1)  ".$space." 2)  ".$block." 3)  ".$left." 4)  ".$right." 5)  ".$center; ?></text>
                <text>Ссылка на slideshow: <a href="https://www.w3schools.com/w3js/tryit.asp?filename=tryw3js_slideshow_next">slideshow</a> </text>
                <center>
                    <form method=POST>
                        <input type=text name=menu value="Menu name"><br> 
                        <textarea type=text name=txt1 style="width: 100%; height: 30vh;">Enter some text</textarea><br>
                        <input type=submit value=Save>
                    </form>
                </center>
            </block>
        </main_block>
    </body>
    <?php
        
        session_start();
        if(!isset($_SESSION['log']))
            header('Location: admin_panel.php');

        if(isset( $_SESSION['lang'])){
            $lang = $_SESSION['lang'];
        }
        else $lang = "ru";
            
        if(isset($_POST['txt1'])){
            $con = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
            $con->query("SET CHARSET UTF-8");
            $str = "
            <space></space>
                ". $_POST['txt1']."
            <space></space>";

            $con->query("INSERT INTO content_".$lang."(Content, type) VALUES('$str', ".strval(CUSTOM).")");
            if(isset($_GET['id']) && $_GET['id'] > 0){
                $id = $_GET['id'];

                $result = $con->query("SELECT * FROM menu_ru WHERE ID = ".$id.";");
                            
                if($result){
                    while($row = $result->fetch_assoc()){
                        $sub_menus = json_decode($row['Sub_menu'], true);
                        $r = $con->query("SELECT ID FROM content_".$lang." WHERE ID = (SELECT MAX(ID) FROM content_".$lang.")");
                        if($r){
                            while($row2 = $r->fetch_assoc()){
                                if(isset($sub_menus["sub"])){
                                    array_push($sub_menus["sub"], array($_POST['menu'], $row2['ID']));
                                } 
                                else{
                                    $new = array("sub" => array(array($_POST['menu'], $row2['ID'])));
                                    $sub_menus = $new;
                                } 
                                $en_arr = json_encode($sub_menus, JSON_UNESCAPED_UNICODE);
                                $con->query("UPDATE menu_".$lang." SET Sub_menu = '$en_arr' WHERE ID = $id;");
                            }
                        }
                    }
                }
            } else $con->query("INSERT INTO menu_".$lang."(Menu, Sub_menu, Content_ID) VALUES('".$_POST['menu']."', '', (SELECT MAX(ID) FROM content_".$lang."))");;
            $con->close();
            header('Location: admin_panel.php');
        }

    ?>
</html>
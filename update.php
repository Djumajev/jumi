<?php 
    session_start();
    if(!isset($_SESSION['log']))
		header('Location: admin.php');

	if(isset( $_SESSION['lang'])){
        $lang = $_SESSION['lang'];
    }
    else $lang = "ru";
		
    header('Content-Type: text/html; charset=utf-8');
	require_once 'db_conf.php';
	require_once 'types_def.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Jumi</title>
		<link rel="stylesheet" href="styles/style.css">
		<link rel="stylesheet" href="styles/inputs.css">
		<link rel="stylesheet" href="styles/media.css">
		<link rel="stylesheet" href="styles/slideshow.css">
		<link rel="stylesheet" href="styles/meet-team.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
		<script>
			function menu(){
				var mob_menu = document.getElementsByTagName('mob_menu');
                var menu = document.getElementById('menu');
				
				if(mob_menu[0].className === ""){
					mob_menu[0].className += "hidden";	
                    menu.className = "";	
				}
				else{
                    mob_menu[0].className = "";
                    menu.className += "hidden";
                } 
			}
		</script>
        <style>
            #content{
                -webkit-user-modify: read-write;
                -moz-user-modify: read-write;
            }
        </style>
	</head>

	<body>
		<main_block style="margin-top: 1vh;">
            <?php
                $con = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
                $con->query("SET CHARSET UTF-8");
                if(isset($_GET['id']))
					$result = $con->query("SELECT * FROM content_".$lang." WHERE ID = ".$_GET['id'].";");
                else $result = $con->query("SELECT * FROM content_".$lang." LIMIT 1;");

                 if($result){
                     while($row = $result->fetch_assoc()){
						 if($row['type'] == CUSTOM) 
						 	echo "<div id=save><textarea style=\"height: 50vh;\">".$row['Content']."</textarea></div>";
						 else echo "<div id=save>".$row['Content']."</div>";
					 }
                 }
            ?>
			<center>
                <form method=POST onsubmit="return getContent()">
                    <textarea id=save-post name=save-post style="display:none"></textarea>
                    <input type=submit value=Save name=save>
					<input type=submit value=Delete name=delete>
                </form>
           </center>
        </main_block>

		<script>
            function getContent(){
                document.getElementById("save-post").value = document.getElementById("save").innerHTML;
			}
        </script>
		<?php
			if(isset($_POST['save-post'], $_GET['id'], $_POST['save'])){
				$id = $_GET['id'];
				$str = $_POST['save-post'];
				$con->query("UPDATE content_".$lang." SET Content = '".$str."' WHERE ID = ".$id.";");

				$con->close();
				header('Location: admin_panel.php');
			}
			elseif(isset($_POST['delete'], $_GET['id'])){
				$id = $_GET['id'];
				$con->query("DELETE FROM content_".$lang." WHERE ID=".$id.";");
				
				if(!isset($_GET['sub'])){
					$con->query("DELETE FROM menu_".$lang." WHERE Content_ID=".$id.";");
				}else {
					$menuID = $_GET['menu_id'];
					$id = $_GET['id'];

					$result = $con->query("SELECT * FROM menu_".$lang." WHERE ID = ".$menuID.";");
                            
                    if($result){
                        while($row = $result->fetch_assoc()){
                            $sub_menus = json_decode($row['Sub_menu'], true);
                            if($sub_menus) foreach ($sub_menus["sub"] as &$value) {
								if($value[1] == $id){
									$value[0] = null;
									$value[1] = null;
								}
							}
							$arr = json_encode($sub_menus);
							$con->query("UPDATE menu_".$lang." SET Sub_menu = '$arr' WHERE ID = $menuID;");
                        }
                    }
				}

				$con->close();
				header('Location: admin_panel.php');
			}


		?>
		<script>
			var img_updates = document.getElementsByClassName("icon_update");
			for(var i = 0; i < img_updates.length; i++){
				img_updates[i].innerHTML = "<form method=GET enctype=\"multipart/form-data\">"  +
                                                "<input type=file id=\"" + img_updates[i].id + "\" onchange=\"handleFiles(this.files, this.id)\"> " + 
                                            "</form>";
			}

			function handleFiles(fileList, ID) {
                var file = fileList[0];

                let formData = new FormData();
                formData.append("uploadFile", file);      

                let xhr = new XMLHttpRequest();
                xhr.open("POST", 'handle_file_upload.php', true);

                xhr.upload.onprogress = function(e) {
                    if (e.lengthComputable) {
                        var percentComplete = (e.loaded / e.total) * 100;
                        console.log(percentComplete + '% uploaded');
                    }
                };
                xhr.onload = function() {
                    if (this.status == 200) {
                        var resp = this.response;
                        console.log('Server got:', resp);
                    }
                };

                xhr.send(formData);

				var el = document.getElementById(ID);
				console.log(ID);
				el.innerHTML = "<a target=\"_blank\" href=./uploads/" + file.name + "><img style=\"width: 100%;\" src=./uploads/" + file.name + "></a>";
            }
		</script>
    </body>
</html>
